package thread2;
import java.util.*;

public class Scheduler extends Thread{
	private int minArrival,maxArrival,minProcessing,maxProcessing;
	private int nrTasks,nrServers;
	private static int simT;
	private static int totalProcessT=0,totalWaitingT=0;
	private static int crtT=0;
	private ArrayList<Task>tArray=new ArrayList<Task>();
	private ArrayList<Server>s=new ArrayList<Server>();
	
	private View v;
	
	Random r=new Random();
	
	public Scheduler(int minA,int maxA,int minP,int maxP,int sT,int nrT,int nrS,View view) {
		minArrival=minA;
		maxArrival=maxA;
		minProcessing=minP;
		maxProcessing=maxP;
		simT=sT;
		nrTasks=nrT;
		nrServers=nrS;
		v=view;
		
		for(int i=0;i<nrServers;i++) {
			s.add(new Server(i));
		}
	}
	public static int getCurrentTime() {
		return crtT;
	}
	
	public static int getSimTime() {
		return simT;
	}
	
	public int getAvgWait() {
		return totalWaitingT/nrTasks;
	}
	
	public int getAvgProcess() {
		return totalProcessT/nrTasks;
	}
	
	public static void addWaitingTime(int tim) {
		totalWaitingT=totalWaitingT+tim;
	}
	
	public String printServers() {
		String st="";
		for(int i=0;i<nrServers;i++) {
			st=st+"Server "+(i+1)+": ";
			for(Task t1:s.get(i).getQ()) {
				st=st+"T["+(t1.getId())+"] ";
			}
			st=st+"\n";
		}
		return st;
	}
	
	public void init(){
		int a=0;
		for(int i=0;i<nrTasks;i++) {
			int aT=a+r.nextInt(maxArrival-minArrival+1)+minArrival;
			a=aT;
			int pT=r.nextInt(maxProcessing-minProcessing+1)+minProcessing;
			Task aux=new Task(i+1,aT,pT);
			tArray.add(aux);
		}
	}
	
	public int getShortest() {
		int min=9999999;
		int index=-1;
		for(Server s1:s) {
			if(s1.getQSize()<min) {
				min=s1.getQSize();
				index=s1.getID();
			}
		}
		return index;
	}
	
	public void run() {
		init();
		for(Server s1:s) {
			s1.start();
		}
		
		for(crtT=1;crtT<=simT;crtT++) {
			View.addInfoArea("Time "+crtT+" ");
			for(Task t1:tArray) {
				if(t1.getStartTime()==crtT) {
					//get it in the shortest queue
					int shr=getShortest();
					s.get(shr).addTask(t1);
					
					totalProcessT=totalProcessT+t1.getProcessingTime();
					
					String st="Task["+ t1.getId()+"], at Server "+ shr+ ", ArrTime:"
						+ t1.getStartTime()+"ProcTime: "+t1.getProcessingTime();
					View.addInfoArea(st);
				}
			}
			
			try {
				Thread.sleep(1000);
			}
			catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
