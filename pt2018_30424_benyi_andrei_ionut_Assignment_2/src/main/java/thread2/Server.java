package thread2;
import java.util.*;

public class Server extends Thread{
	private LinkedList<Task> q;
	private int qEmptyT;
	private int id;
	
	public Server(int i) {
		this.id=i;
		this.q=new LinkedList<Task>();
		qEmptyT=0;
	}
	
	//get object's attributes
	public LinkedList<Task> getQ(){
		return this.q;
	}
	
	public int getID() {
		return this.id;
	}
	
	public int getQSize() {
		return q.size();
	}
	
	//add and remove Tasks
	public void addTask(Task t) {
		this.q.add(t);
	}
	
	public void removeTask() {
		this.q.remove();
	}
	
	public void run() {
		while(Scheduler.getCurrentTime()<Scheduler.getSimTime()){
			if(q.size()>0) {
				//if we have tasks in the server, we put the thread to sleep
				//sleep time=the processing time of the first task from the server
				try {
					Thread.sleep(1000*q.getFirst().getProcessingTime());
					View.addInfoArea("Task "+q.getFirst().getId()+" left at "+Scheduler.getCurrentTime());
				
				}
				catch(InterruptedException e) {
					e.printStackTrace();
				}finally {
					//Task completed, so we remove it
					Scheduler.addWaitingTime(Scheduler.getCurrentTime()-q.getFirst().getStartTime()-q.getFirst().getProcessingTime());
					q.removeFirst();
				}
			}
			else {
				//increment the server empty time
				qEmptyT++;
				try {
					Thread.sleep(1000);
				}catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

