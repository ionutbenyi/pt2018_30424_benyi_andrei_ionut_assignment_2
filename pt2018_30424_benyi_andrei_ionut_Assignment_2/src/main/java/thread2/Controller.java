package thread2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

public class Controller {
	
	private View v;
	private Scheduler sched;
	
	public Controller(View v1) {
		this.v=v1;
		
		v.runAction(new RunListener());
		v.analysisAction(new AnalysisListener());
	}
	
	class RunListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if((v.arrMinTF.getText().isEmpty()) || (v.arrMaxTF.getText().isEmpty()) || (v.prMinTF.getText().isEmpty()) || (v.prMaxTF.getText().isEmpty()) || (v.queuesNrTF.getText().isEmpty()) || (v.tasksNrTF.getText().isEmpty()) || (v.runTimeTF.getText().isEmpty())) {
				JOptionPane.showMessageDialog(null, "Error. Check the parameters again");
			}
			else {
				int minArrivalTime=Integer.parseInt(v.getArrMin());
				int maxArrivalTime=Integer.parseInt(v.getArrMax());
				int minProcessTime=Integer.parseInt(v.getPrMin());
				int maxProcessTime=Integer.parseInt(v.getPrMax());
				int nrTasks=Integer.parseInt(v.getTasksNr());
				int nrServers=Integer.parseInt(v.getQueuesNr());
				int simulationTime=Integer.parseInt(v.getRunTime());
			
				sched=new Scheduler(minArrivalTime,maxArrivalTime,minProcessTime,maxProcessTime,simulationTime,nrTasks,nrServers,v);
				sched.start();
				
				Timer t=new Timer();
				MyTimer tt=new MyTimer(sched,v);
				t.scheduleAtFixedRate(tt,0,1000);
			}
		}
	}
	
	class AnalysisListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String fin="Average processing time: "+sched.getAvgProcess()+"\n"+"Average waiting time: "+sched.getAvgWait()+"\n";
			JOptionPane.showMessageDialog(null, fin);
		}
	}
}