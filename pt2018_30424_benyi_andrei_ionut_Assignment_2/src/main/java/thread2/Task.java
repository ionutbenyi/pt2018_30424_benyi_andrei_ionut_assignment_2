package thread2;
import java.util.*;
public class Task {
	private int id;
	private int startTime;
	private int processingTime;
	
	public Task(int i,int s, int p)
	{
		this.id=i;
		this.startTime=s;
		this.processingTime=p;
	}
	
	public int getId() {
		return this.id;
	}
	
	public int getStartTime() {
		return this.startTime;
	}
	
	public int getProcessingTime() {
		return this.processingTime;
	}
}
