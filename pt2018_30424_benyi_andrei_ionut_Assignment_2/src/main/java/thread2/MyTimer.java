package thread2;
import java.util.*;
public class MyTimer extends TimerTask{
	
	private Scheduler sc;
	private View v;
	
	public MyTimer(Scheduler s,View v1) {
		this.sc=s;
		this.v=v1;
	}
	
	public void run() {					
		v.setActualArea(sc.printServers());
	}
}
